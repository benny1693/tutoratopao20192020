# Tutorato di Programmazione a Oggetti (A.A. 2019-2020)
Repository del tutorato del corso di Programmazione a Oggetti (A.A. 2019-2020) dell'Università degli Studi di Padova. Lo scopo del tutorato è illustrare come sviluppare una piccola applicazione desktop con le librerie messe a disposizione da Qt. 

## Il Gioco del Tris (TicTacToe)

Durante gli incontri di laboratorio, realizzeremo il Gioco del Tris suddividendo l'applicazione in tre parti distinte:
- Modello
- Controller
- Vista

Controller e Vista useranno librerie di Qt per gestire l'interfaccia grafica e la gestione degli eventi (tramite il sistema di segnali e slot).
Il Modello, invece, sarà assolutamente indipendente dalle librerie Qt e dovrà poter essere utilizzato in una qualsiasi applicazione.
