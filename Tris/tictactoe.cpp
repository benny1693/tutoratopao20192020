#include "tictactoe.h"
#include "controller.h"

TicTacToe::TicTacToe(Controller* c, QWidget *parent) : QWidget(parent), controller(c) {
	mainLayout = new QVBoxLayout(this);
	gridLayout = new QGridLayout();

	// Aggiungo il menu
	addMenu();
	// Aggiungo le celle -> bottoni
	addButtons();

	// Imposto lo stile
	setTicTacToeStyle();

	// Aggiungere i layout
	mainLayout->addLayout(gridLayout);
	setLayout(mainLayout);
}

void TicTacToe::update(){
	// Per adesso l'argomento player viene utilizzato per il testing
	// In futuro verrà preso dal controller
	for (unsigned short i = 0U; i < 9; ++i){

		int player = controller->getPlayer(i/3,i%3);
		// Se la cella è occupata da qualcuno
		if (player != 0){
			// Considero il bottone corrispondente
			QLayoutItem* item = gridLayout->itemAtPosition(i/3,i%3);
			QPushButton* button = static_cast<QPushButton*>(item->widget());
			// Se la cella è occupata dal giocatore 1
			if (player == 1){
				button->setIcon(QIcon(":/resources/player1.png"));
			} else { // Se è occupata dal giocatore 2
				button->setIcon(QIcon(":/resources/player2.png"));
			}
			// Impedisco future interazioni con il tasto
			button->setEnabled(false);
		}
	}
}

void TicTacToe::resetGrid(){
	for (unsigned short int i = 0U; i < 9; i++) {
		// "Estraggo" il bottone
		QPushButton *button = static_cast<QPushButton*>(gridLayout->itemAtPosition(i / 3, i % 3)->widget());

		// Tolgo l'icona
		button->setIcon(QIcon());

		// Riabilito il bottone
		button->setEnabled(true);
	}
}

void TicTacToe::showWinner(){
	// Creo la finestra col messaggio con QDialog
	QDialog* dialog = new QDialog(this);
	QVBoxLayout* layout = new QVBoxLayout(dialog);

	// Creo il messaggio
	int winner = controller->getWinner();
	std::stringstream text;
	if (winner)
		text << "Ha vinto il giocatore" << winner;
	else
		text << "Pareggio";

	// Inserisco il messaggio nella finestra con QLabel
	layout->addWidget(new QLabel(QString::fromStdString(text.str()),dialog));

	// Mostrare la finestra
	dialog->show();
}

void TicTacToe::showErrorMessage(const QString& message) {
	// Creo la finestra col messaggio con QDialog
	QDialog* dialog = new QDialog(this);
	QVBoxLayout* layout = new QVBoxLayout(dialog);

	// Inserisco il messaggio nella finestra con QLabel
	layout->addWidget(new QLabel(message,dialog));

	// Mostrare la finestra
	dialog->show();
}

void TicTacToe::cellHandler(unsigned short x, unsigned short y) const{
	/* altre possibili azioni */
}

void TicTacToe::addButtons(){

	for (unsigned short int i = 0U; i < 9; i++){
		// Creo un bottone -> QPushButton
		TicTacToeButton* button = new TicTacToeButton(i/3,i%3,this);

		// Dare una policy per le dimensioni
		button->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

		// Connetto l'evento del bottone alle varie azioni da svolgere
		connect(button,SIGNAL(clickedCell(unsigned short,unsigned short)),this,SLOT(cellHandler(unsigned short, unsigned short)));
		connect(button,SIGNAL(clickedCell(unsigned short,unsigned short)),controller,SLOT(move(unsigned short, unsigned short)));

		// Aggiungere al layout
		gridLayout->addWidget(button,i/3,i%3);
	}
}

void TicTacToe::addMenu(){
	// Creare la barra dei menu, poi il menu e infine le azioni
	QMenuBar* menubar = new QMenuBar(this);
	QMenu* menu = new QMenu("File",menubar);
	QAction* exit = new QAction("Exit",menu);
	QAction* reset = new QAction("Reset",menu);

	connect(reset,SIGNAL(triggered()),controller,SLOT(resetGame()));
	connect(exit,SIGNAL(triggered()),this,SLOT(close()));

	// Aggiungo le azioni al menu
	menu->addAction(reset);
	menu->addAction(exit);

	// Aggiungo il menu alla barra
	menubar->addMenu(menu);

	// Aggiungere la barra al layout
	mainLayout->addWidget(menubar);
}

void TicTacToe::setTicTacToeStyle()
{
	gridLayout->setSpacing(0);

	// Imposto le dimensioni
	setMinimumSize(QSize(400,400));
	setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

	// Imposto il foglio di stile
	QFile file(":/resources/style.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());

	setStyleSheet(styleSheet);
}
