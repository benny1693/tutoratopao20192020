#ifndef TICTACTOEBUTTON_H
#define TICTACTOEBUTTON_H

#include <QPushButton>

class TicTacToeButton : public QPushButton {
	Q_OBJECT
public:
	/**
	 * @brief TicTacToeButton costruttore del bottone della cella per il gioco del
	 *												Tris in posizione (x,y)
	 * @param x posizione in altezza del bottone
	 * @param y posizione in larghezza del bottone
	 * @param parent widget genitore
	 */
	TicTacToeButton(unsigned short x, unsigned short y, QWidget* parent = nullptr);

signals:
	/**
	 * @brief clickedCell segnale per riemettere clicked indicando anche
	 *										la posizione del bottone
	 */
	void clickedCell(unsigned short, unsigned short);

private:
	unsigned short x,y;

private slots:
	/**
	 * @brief cellEmitter slot che emette clickedCell con this->x e this->y
	 */
	void cellEmitter();
};

#endif // TICTACTOEBUTTON_H
