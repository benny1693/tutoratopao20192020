#include "tictactoebutton.h"

TicTacToeButton::TicTacToeButton(unsigned short X, unsigned short Y, QWidget* parent) : QPushButton(parent), x(X), y(Y) {
	// Connetto il segnale di QPushButton allo slot cellEmitter
	connect(this,SIGNAL(clicked()),this,SLOT(cellEmitter()));
}

void TicTacToeButton::cellEmitter(){
	// Emetto il segnale indicando le coordinate del bottone
	emit clickedCell(x,y);
}
