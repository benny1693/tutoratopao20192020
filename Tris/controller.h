#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include "tictactoe.h"
#include "model.h"

class Controller : public QObject {
	Q_OBJECT
 public:
	/**
	 * @brief Controller	costruttore del controller
	 * @param model				puntatore al modello a cui dare le indicazioni
	 * @param parent			widget genitore
	 */
	explicit Controller(Model* model, QObject *parent = nullptr);

	/**
	 * @brief setView		imposta la vista a cui dare le indicazioni
	 * @param v					puntatore alla vista
	 */
	void setView(TicTacToe* v);

	/**
	 * @brief getPlayer	 restituisce il giocatore che si trova nella posizione richiesta
	 * @param x					 indice di riga
	 * @param y          indici di colonna
	 * @return					 l'indice del giocatore, cioè
	 *											0 sse nessun giocatore ha occupato la posizione (x,y)
	 *											1 sse il giocatore 1 ha occupato la posizione (x,y)
	 *											2 sse il giocatore 2 ha occupato la posizione (x,y)
	 */
	int getPlayer(unsigned short x, unsigned short y) const;

	/**
	 * @brief getWinner		restituisce il giocatore che ha vinto la partita
	 * @return					 l'indice del vincitore, cioè
	 *											0 sse nessun giocatore ha vinto
	 *											1 sse il giocatore 1 ha vinto
	 *											2 sse il giocatore 2 ha vinto
	 */
	int getWinner() const;

 public slots:

	/**
	 * @brief move	permette l'esecuzione di una mossa in base agli indici forniti
	 * @param x			indice di riga
	 * @param y			indice di colonna
	 */
	void move(unsigned short row, unsigned short col);

	/**
	 * @brief resetGame reimposta il campo da gioco di vista e modello
	 */
	void resetGame();

 private:
	TicTacToe* view;
	Model* model;
};

#endif  // CONTROLLER_H
