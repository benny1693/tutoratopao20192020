#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QMenuBar>
#include <QPushButton>
#include <QIcon>
#include <QFile>
#include <QDialog>
#include <QLabel>

#include <sstream>
#include "tictactoebutton.h"

class Controller;

class TicTacToe : public QWidget {
	Q_OBJECT

 public:
	TicTacToe(Controller* controller, QWidget *parent = nullptr);
	~TicTacToe() = default;

	/**
	 * @brief update aggiorna la vista (il parametro int è provvisorio)
	 */
	void update();

	/**
	 * @brief resetGrid effettua il reset della vista
	 */
	void resetGrid();

	/**
	 * @brief showWinner mostra il vincitore della partita
	 */
	void showWinner();

	/**
	 * @brief showErrorMessage
	 * @param message
	 */
	void showErrorMessage(const QString& message);

 public slots:
	void cellHandler(unsigned short, unsigned short) const;

 private:
	Controller* controller;

	QVBoxLayout *mainLayout;
	QGridLayout *gridLayout;

	/**
	 * @brief addButtons aggiunge i bottoni alla vista
	 */
	void addButtons();

	/**
	 * @brief addMenu aggiunge il menu alla vista
	 */
	void addMenu();

	/**
	 * @brief setTicTacToeStyle imposta lo stile dell'applicazione
	 */
	void setTicTacToeStyle();
};
#endif  // TICTACTOE_H
