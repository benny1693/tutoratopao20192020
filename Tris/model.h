#ifndef MODEL_H
#define MODEL_H

#include <stdexcept>
#include "moveexception.h"

#define u_int unsigned short

enum Player { none = 0, player1 = 1, player2 = 2 };

class Model {
 public:
	/**
	 * @brief TicTacToeModel costruttore di default del modello
	 */
	Model();

	/**
	 * @brief move		inserisce nella cella indicata la mossa del giocatore scelto
	 * @param x				indice di riga
	 * @param y				indice di colonna
	 * @throw std::exception
	 */
	void move(u_int x, u_int y);

	/**
	 * @brief winner restituisce il vincitore se esiste
	 * @return player1 sse il vincitore è il primo giocatore,
	 *				 player2 sse il vincitore è il secondo
	 *										 giocatore,
	 *				 none altrimenti
	 */
	Player winner() const;

	/**
	 * @brief reset prepara il modello a una nuova partita
	 */
	void reset();

	/**
	 * @brief countFrame	conta il numero di celle occupate dal giocatore indicato
	 * @param player			giocatore
	 * @return (int)			numero di celle occupate da player
	 */
	unsigned short int countFrame(Player player) const;

	/**
	 * @brief operator()	operatore di accesso ai valori del campo da gioco
	 * @param row					indice di riga
	 * @param column			indice di colonna
	 * @return (Player)		restituisce il valore della cella in riga row e colonna col
	 */
	Player operator()(unsigned short int row, unsigned short int column) const;


private:
	Player grid[9];
	bool turn;

	/**
	 * @brief checkWinner controlla che nelle tre posizioni indicate ci sia lo
	 *										stesso giocatore
	 * @param i						indice della prima cella
	 * @param j						indice della seconda cella
	 * @param k						indice della terza cella
	 * @return restistuisce true sse field[i] = field[j] = field[k] != none
	 */
	bool checkWinner(u_int i, u_int j, u_int k) const;
};

#endif  // MODEL_H
