#include "moveexception.h"

MoveException::MoveException(unsigned short X, unsigned short Y)
		: std::runtime_error("Exception: the move in (" + std::to_string(X) + "," +
												 std::to_string(Y) + ") cannot be done"),
			x(X),
			y(Y) {}
