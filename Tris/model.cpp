#include "model.h"

Model::Model() { reset(); }

void Model::move(unsigned short x, unsigned short y) {
	// Bisogna controllare i limiti di x e y
	if (x > 2 && y > 2)
		throw new std::domain_error("Exception: (" + std::to_string(x) + "," + std::to_string(y) + ") out of grid's bounds");

	// Se c'è un vincitore la mossa non è valida
	if (winner()) throw new MoveException(x,y);

	// Se la cella è già occupata la mossa non è valida
	if (grid[x * 3 + y]) throw new MoveException(x,y);

	// Eseguo la mossa (che è valida)
	grid[x * 3 + y] = (turn ? player2 : player1);
	turn = !turn;
}

Player Model::winner() const {
	for (u_int i = 0; i < 3; i++) {
		// righe
		if (checkWinner(i * 3, i * 3 + 1, i * 3 + 2))
			return grid[i * 3];

		// colonne
		if (checkWinner(i, 3 + i, 6 + i))
			return grid[i];
	}

	// diagonali
	if (checkWinner(0, 4, 8))
		return grid[0];

	if (checkWinner(2, 4, 6))
		return grid[2];

	return none;
}

void Model::reset() {
	turn = 0;
	for (u_int i = 0; i < 9; i++) grid[i] = none;
}

unsigned short Model::countFrame(Player player) const {
	u_int counter = 0U;

	for (u_int i = 0U; i < 9; ++i)
		counter += (grid[i] == player);

	return counter;
}

Player Model::operator()(unsigned short row, unsigned short column) const {
	if (row > 2 && column > 2)
		return none;
	return grid[row*3+column];
}

bool Model::checkWinner(unsigned short i, unsigned short j,
												unsigned short k) const {
	return grid[i] && (grid[i] == grid[j]) && (grid[j] == grid[k]);
}
