#include "controller.h"

Controller::Controller(Model* m, QObject* parent)
		: QObject(parent), view(nullptr), model(m) {}

void Controller::setView(TicTacToe* v) { view = v; }

int Controller::getPlayer(unsigned short x, unsigned short y) const {
	return (*model)(x, y);
}

int Controller::getWinner() const { return model->winner(); }

void Controller::move(unsigned short row, unsigned short col) {

	try{
		// Proviamo a eseguire la mossa
		model->move(row,col);

		// Aggiorniamo la vista
		view->update();

		// Se esiste un vincitore o c'è un pareggio, allora bisogna mostrarlo e reimpostare il gioco
		if (model->winner() || (model->countFrame(player1) + model->countFrame(player2) == 9)){
			view->showWinner();
			resetGame();
		}
	} catch (std::exception* exc) {
		view->showErrorMessage(exc->what());
	}
}

void Controller::resetGame() {
	// Reimpostare il modello
	model->reset();

	// Reimpostare la vista
	view->resetGrid();
}
